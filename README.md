## About The Project

[Sliide App](https://sliide-app.web.app/)

This app uses npm as a package manager

## Utilities:

Utilities
This turborepo has some additional tools already setup for you:

- [TypeScript](https://www.typescriptlang.org/) for static type checking
- [ESLint](https://eslint.org/) for code linting
- [Prettier](https://prettier.io/) for code formatting

## Recommendations:

- Install [node version manager](https://github.com/nvm-sh/nvm)
- Use [VS Code](https://code.visualstudio.com/) as an IDE for auto installation of extensions for auto formatting and linting with Typescript

## Getting Started

### Prerequisites

- npm
  ```sh
  npm install npm@latest -g
  ```
- Must use [npm](https://www.npmjs.com/) as package manager

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/your_username_/Project-Name.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Run the webapp
   ```sh
   npm run dev
   ```

### Running Tests

1. Make sure the playwright executable is installed on your machine
   ```sh
    npx playwright install
   ```
2. Make sure the webapp is running
   ```sh
   npm run dev
   ```
3. Run test script
   ```sh
   npm run test
   ```
4. View test results
   ```sh
   npm run test:view
   ```
