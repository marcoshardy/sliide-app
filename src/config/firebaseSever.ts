import admin, { ServiceAccount } from 'firebase-admin';

import serviceAccount from '../../sliide-firebase-pkey.json';

if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount as ServiceAccount),
  });
}

const adminFirestore = admin.firestore();
const adminAuth = admin.auth();

export { adminFirestore, adminAuth };
