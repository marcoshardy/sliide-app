import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
  authDomain: 'sliide-app.firebaseapp.com',
  projectId: 'sliide-app',
  storageBucket: 'sliide-app.appspot.com',
  messagingSenderId: '316355942418',
  appId: '1:316355942418:web:d2b23259f6aba192f720cb',
  measurementId: 'G-32J3HC9YVX',
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
