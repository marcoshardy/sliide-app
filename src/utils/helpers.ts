import { auth } from '@/config/firebaseClient';
import { User } from '@/types';
import { AppPackage } from '@/types/apps';
import { getDownloadURL, getStorage, ref } from '@firebase/storage';
import axios from 'axios';
import crypto from 'crypto';
import { enqueueSnackbar } from 'notistack';
import { validateAppPackage } from './forms/forms';

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export const VIEWPORT_HEIGHT = 'calc(100vh - 9rem)';

export const setTokenInLocalStorage = (token: string) => {
  window.localStorage.setItem('token', token);
};

export const removeTokenFromLocalStorage = () => {
  window.localStorage.removeItem('token');
};

export const getIdToken = async () => {
  if (!auth.currentUser) console.error('no currentUser');
  return await auth.currentUser?.getIdToken();
};

export const getUserByToken = async () => {
  try {
    const idToken = await getIdToken();
    const resp = await axios.get('/api/user', {
      headers: {
        token: idToken,
      },
    });

    const data = resp.data;

    if (data && 'name' in data && 'uid' in data && 'email' in data) {
      data.firebaseToken = idToken;
      return data as User;
    }
    return null;
  } catch (error) {
    console.error('Error getting user by token:', error);
    return null;
  }
};

export const getAllApps = async () => {
  try {
    const idToken = await getIdToken();
    const resp = await axios.get('/api/apps', {
      headers: {
        token: idToken,
      },
    });

    const data = resp.data;

    if (data) {
      return data as AppPackage[];
    }
    return null;
  } catch (error) {
    console.error('Error getting apps:', error);
    return null;
  }
};

export const getAnApp = async (appId: string) => {
  try {
    const idToken = await getIdToken();
    const resp = await axios.get('/api/app', {
      headers: {
        token: idToken,
      },
      params: {
        appId,
      },
    });

    const data = resp.data;

    if (data) {
      return data as AppPackage;
    }
    return null;
  } catch (error) {
    console.error('Error getting app:', error);
    return null;
  }
};

export const deleteAnApp = async (appId: string) => {
  try {
    const idToken = await getIdToken();
    await axios.delete('/api/app', {
      headers: {
        token: idToken,
      },
      params: {
        appId,
      },
    });

    return true;
  } catch (error) {
    console.error('Error deleting app:', error);
    return null;
  }
};

export const updateAppPackage = async (appPackage: AppPackage) => {
  const validationResp = validateAppPackage(appPackage, true);

  if (validationResp.isValid) {
    try {
      const idToken = await getIdToken();
      await axios.put(
        '/api/app',
        {
          appPackage,
        },
        {
          headers: {
            token: idToken,
          },
        }
      );

      return true;
    } catch (error) {
      console.error('Error updating app:', error);
      return null;
    }
  } else {
    enqueueSnackbar(validationResp.errorMsg, {
      variant: 'error',
    });
    return null;
  }
};

export const randomId = (numBytes: number) => {
  return crypto.randomBytes(numBytes).toString('hex');
};

export const getApkDownloadUrl = async (appId: string) => {
  try {
    const storage = getStorage();

    const apkFileRef = ref(storage, `apps/${appId}/${appId}.apk`);
    const downloadURL = await getDownloadURL(apkFileRef);

    if (downloadURL) {
      return downloadURL;
    }
    return false;
  } catch (error) {
    console.error('File unavailable', error);
    return false;
  }
};
