import { auth } from '@/config/firebaseClient';
import { AppPackage } from '@/types/apps';
import axios from 'axios';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { enqueueSnackbar } from 'notistack';
import { getIdToken } from '@/utils/helpers';

export const signUpUser = (email: string, password: string) => {
  createUserWithEmailAndPassword(auth, email, password)
    .then(() => {
      enqueueSnackbar('Successfully signed in', { variant: 'success' });
    })
    .catch((error) => {
      const errorMessage = error.message;

      enqueueSnackbar(`${errorMessage}`, { variant: 'error' });

      // ..
    });
};

export const createNewAppPackage = async (appPackage: AppPackage) => {
  try {
    const idToken = await getIdToken();

    const resp = await axios.post(
      'api/app',
      {
        appPackage,
      },
      {
        headers: {
          token: idToken,
        },
      }
    );

    if (resp.data) {
      enqueueSnackbar('Successfully created new app package', {
        variant: 'success',
      });
      return resp.data as AppPackage;
    } else {
      throw new Error('Created app package was not returned from server');
    }
  } catch (error) {
    console.error('error:', error);
    enqueueSnackbar('Something went wrong, Please try again', {
      variant: 'error',
    });
    return false;
  }
};

interface FormError {
  isValid: boolean;
  errorMsg: string | null;
}

export const validateAppPackage = (appPackage: AppPackage, validateAllFields?: boolean): FormError => {
  if (!appPackage?.appName || appPackage?.appName.length < 2) {
    return {
      isValid: false,
      errorMsg: 'App Name must exist and be longer than 2 characters',
    };
  }

  if (!appPackage?.bundleId || appPackage?.bundleId.length < 5) {
    return {
      isValid: false,
      errorMsg: 'App Bundle ID must exist and be longer than 5 characters',
    };
  }

  if (!appPackage?.version || String(appPackage?.version).length < 2) {
    return {
      isValid: false,
      errorMsg: 'App Version must exist and be longer than 2 characters',
    };
  }

  if (!appPackage?.status) {
    return {
      isValid: false,
      errorMsg: 'Invalid app status',
    };
  }

  if (validateAllFields) {
    if (appPackage?.notes && appPackage?.notes.length > 0) {
      const isEveryNoteValid = appPackage.notes.every((note) => {
        if (!note?.text || note.text.length < 1) {
          return false;
        }

        if (!note?.createdAt) {
          return false;
        }
        if (!note?.userId) {
          return false;
        }
        return true;
      });

      if (!isEveryNoteValid) {
        return {
          isValid: false,
          errorMsg: 'Invalid note',
        };
      }
    }

    if (!appPackage?.id) {
      return {
        isValid: false,
        errorMsg: 'Invalid app ID',
      };
    }
  }

  return { isValid: true, errorMsg: null };
};
