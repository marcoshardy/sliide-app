import { Loader } from '@/components';
import { SignUpForm } from '@/components/Forms';
import { LayoutProps } from '@/components/Layout';
import { useAuthContext } from '@/context/AuthContext/AuthContext';
import { VIEWPORT_HEIGHT } from '@/utils/helpers';
import { Box } from '@mui/system';
import React from 'react';

export const SignUp: React.FC<LayoutProps> = () => {
  const { isLoading } = useAuthContext();
  return (
    <Box height={VIEWPORT_HEIGHT}>
      {isLoading && <Loader />}

      <SignUpForm />
    </Box>
  );
};
