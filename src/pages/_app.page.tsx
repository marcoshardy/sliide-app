import React from 'react';
import type { ReactElement, ReactNode } from 'react';
import type { NextPage } from 'next';
import type { AppProps } from 'next/app';
import Layout from '@/components/Layout';
import { AuthProvider } from '@/context/AuthContext/AuthContext';
import { SnackbarProvider } from 'notistack';
import { AppProvider } from '@/context/AppContext/AppContext';

export type NextPageWithLayout<P = object, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

export default function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  return (
    <AuthProvider>
      <AppProvider>
        <Layout>
          <SnackbarProvider>
            <Component {...pageProps} />
          </SnackbarProvider>
        </Layout>
      </AppProvider>
    </AuthProvider>
  );
}
