import { useAuthContext, useAppContext } from '@/context';
import React, { useEffect, useState } from 'react';

import { Button, Grid, Box, Typography, TextField, InputAdornment } from '@mui/material';
import type { GridProps } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { AppDirectoryTable, Loader } from '@/components';
import { AppPackage } from '@/types/apps';
import { LayoutProps } from '@/components/Layout';

export const Home: React.FC<LayoutProps> = () => {
  const [filterQuery, setFilterQuery] = useState('');
  const [filteredApps, setFilteredApps] = useState<AppPackage[] | null>(null);

  const { user } = useAuthContext();

  const { isLoading, apps } = useAppContext();

  useEffect(() => {
    if (filterQuery.length > 0) {
      setFilteredApps(apps.filter((a) => a.appName.toLowerCase().includes(filterQuery.toLowerCase())));
    } else {
      if (filteredApps) {
        setFilteredApps(null);
      }
    }
  }, [filterQuery]);

  if (!user) {
    return null;
  }

  return (
    <Box
      sx={{
        bgcolor: 'background.paper',

        p: '5%',
      }}
    >
      {isLoading && <Loader />}
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h3" align="left">
            App Packages
          </Typography>
        </Grid>
        <Grid {...TopContainer}>
          <TextField
            id="input-with-icon-textfield"
            label="Search"
            size="small"
            value={filterQuery}
            onChange={(e) => {
              setFilterQuery(e.target.value);
            }}
            sx={{ mr: '2rem', minWidth: '10rem' }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
            variant="outlined"
          />

          <Button variant="contained" size="medium">
            Search
          </Button>
        </Grid>
        <Grid {...AddPackageBtnContainer}>
          <Button href="/new-app-package" variant="contained">
            Add Package
          </Button>
        </Grid>

        <Grid item xs={12}>
          {apps && filteredApps ? <AppDirectoryTable apps={filteredApps} /> : <AppDirectoryTable apps={apps} />}
        </Grid>
      </Grid>
    </Box>
  );
};

const TopContainer: GridProps = {
  item: true,
  xs: 4,
  height: '4rem',
  width: '100%',
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
};
const AddPackageBtnContainer: GridProps = {
  item: true,
  xs: 8,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
};
