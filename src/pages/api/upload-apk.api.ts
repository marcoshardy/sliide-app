import { NextApiRequest, NextApiResponse } from 'next';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'POST') {
    res.status(405).json({ message: 'Method not allowed' });
    return;
  }

  try {
    const file = req.body?.apk;
    const maxFileSize = 100 * 1024 * 1024; // 100MB

    if (!file) {
      res.status(400).json({ message: 'No file uploaded' });
      return;
    }

    if (file.size > maxFileSize) {
      res.status(400).json({ message: 'File size exceeds the maximum limit' });
      return;
    }

    res.status(200).json({ message: 'File uploaded successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};
