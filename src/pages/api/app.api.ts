import type { NextApiRequest, NextApiResponse } from 'next';
import { AppPackage } from '@/types/apps';
import {
  deleteAnAppFromFirestore,
  getAnAppFromFirestore,
  updateAnAppFromFirestore,
  verifyToken,
  addAppPackageToFirestore,
} from '@/pages/api/utils/utils';
import { validateAppPackage } from '@/utils/forms/forms';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  await verifyToken(req, res);

  if (req.method === 'POST') {
    try {
      const { appPackage } = req.body;

      if (!appPackage || !validateAppPackage(appPackage, true).isValid) {
        res.status(401).json({ message: 'Invalid app package', code: 401 });
      }

      const appPackageData: AppPackage = appPackage;

      const resp = await addAppPackageToFirestore(appPackageData);

      if (resp) {
        res.status(200).json(appPackageData);
      } else {
        throw new Error('Could not set new app package data in firestore');
      }
    } catch (error) {
      console.error('error:', error);
      res.status(500).json({
        message: 'Error creating app package. Please try again',
        code: 500,
      });
    }
  }

  if (req.method === 'GET') {
    try {
      const { appId } = req.query;

      if (!appId) {
        res.status(400).json({
          message: 'Invalid App ID',
          code: 400,
        });
      }

      const app = await getAnAppFromFirestore(appId as string);

      if (app) {
        res.status(200).json(app);
      } else {
        throw new Error('Could not get app from firestore');
      }
    } catch (error) {
      console.error('error:', error);
      res.status(500).json({
        message: 'Error fetching app. Please try again',
        code: 500,
      });
    }
  }
  if (req.method === 'DELETE') {
    try {
      const { appId } = req.query;

      if (!appId) {
        res.status(400).json({
          message: 'Invalid App ID',
          code: 400,
        });
      }

      const resp = await deleteAnAppFromFirestore(appId as string);

      if (resp) {
        res.status(200).json('ok');
      } else {
        throw new Error('Could not delete app from firestore');
      }
    } catch (error) {
      console.error('error:', error);
      res.status(500).json({
        message: 'Error delete app. Please try again',
        code: 500,
      });
    }
  }

  if (req.method === 'PUT') {
    try {
      const { appPackage } = req.body;

      if (!appPackage || !validateAppPackage(appPackage, true).isValid) {
        res.status(400).json({
          message: 'Invalid updated app package',
          code: 400,
        });
      }

      const updatedAppPackage = appPackage as AppPackage;

      const resp = await updateAnAppFromFirestore(updatedAppPackage);

      if (resp) {
        res.status(200).json('ok');
      } else {
        throw new Error('Could not update app in firestore');
      }
    } catch (error) {
      console.error('error:', error);
      res.status(500).json({
        message: 'Error updating app. Please try again',
        code: 500,
      });
    }
  }
};
