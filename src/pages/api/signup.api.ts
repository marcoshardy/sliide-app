import { User, ApiErrorResp } from '@/types';
import type { NextApiRequest, NextApiResponse } from 'next';
import { adminAuth, adminFirestore } from '@/config/firebaseSever';

export default async (req: NextApiRequest, res: NextApiResponse<ApiErrorResp | User>) => {
  try {
    const { name, email, password } = req.body;

    if (!email || !password || !name) {
      res.status(400).json({ message: 'Email, name or password missing', code: 400 });
    }

    const userRecord = await adminAuth.createUser({
      email,
      password,
      displayName: name,
      emailVerified: false,
      disabled: false,
    });

    // See the UserRecord reference doc for the contents of userRecord.

    const user: Omit<User, 'firebaseToken'> = {
      createdAt: new Date(),
      updatedAt: new Date(),
      email,
      name,
      uid: userRecord.uid,
    };

    if (userRecord) {
      await addUserToFirestore(user);

      const token = await adminAuth.createCustomToken(user.uid);

      const fullUser: User = { ...user, firebaseToken: token };

      res.status(200).json(fullUser);
    } else {
      throw new Error('Could not create user');
    }
  } catch (error) {
    console.error('error:', error);
    res.status(500).json({ message: 'Error creating user. Please try again', code: 500 });
  }
};

const addUserToFirestore = async (user: Omit<User, 'firebaseToken'>) => {
  try {
    await adminFirestore.collection('users').doc(user.uid).set(user);
    return true;
  } catch (error) {
    console.error('add user to firestore error:', error);
    return false;
  }
};
