import { adminAuth, adminFirestore } from '@/config/firebaseSever';
import { User } from '@/types';
import { AppPackage } from '@/types/apps';
import { Timestamp } from '@firebase/firestore';
import { NextApiRequest, NextApiResponse } from 'next';

export const deserializeApp = (app: AppPackage) => {
  const firestoreDate = app.createdAt as Timestamp;
  app.createdAt = firestoreDate.toDate();
  return app;
};
export const serializeApp = (app: AppPackage) => {
  app.createdAt = new Date(app.createdAt as Date);
  return app;
};

export const verifyToken = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { token } = req.headers;

    if (!token) {
      throw new Error('no token in query');
    }

    const decodedToken = await adminAuth.verifyIdToken(token as string);
    return decodedToken.uid;
  } catch (error) {
    console.error('invalid token');
    res.status(401).json({ message: 'Invalid Token', code: 401 });
  }
};

export const getUserFromFirestore = async (uid: string) => {
  try {
    const user = await adminFirestore.collection('users').doc(uid).get();

    if (user.exists) {
      return user.data() as User;
    }
    return null;
  } catch (error) {
    console.error(error);
    return null;
  }
};

export const getAppsFromFirestore = async () => {
  try {
    const stream = await adminFirestore.collection('apps').orderBy('createdAt', 'desc').get();

    const apps: AppPackage[] = [];

    stream.forEach((snapshot) => {
      if (snapshot.exists) {
        const data = deserializeApp(snapshot.data() as AppPackage);
        apps.push(data);
      }
    });

    return apps;
  } catch (error) {
    console.error('error fetching app packages from firestore:', error);
    return false;
  }
};

export const getAnAppFromFirestore = async (appId: string) => {
  try {
    const app = await adminFirestore.collection('apps').doc(appId).get();

    if (app.exists) {
      const appData = app.data() as AppPackage;
      return deserializeApp(appData);
    }
    console.error('app does not exist in firestore');

    return null;
  } catch (error) {
    console.error('getAnAppFromFirestore Error:', error);
    return null;
  }
};

export const deleteAnAppFromFirestore = async (appId: string) => {
  try {
    await adminFirestore.collection('apps').doc(appId).delete();

    return true;
  } catch (error) {
    console.error(error);
    return null;
  }
};

export const updateAnAppFromFirestore = async (appPackage: AppPackage) => {
  try {
    const serializedAppPackage = serializeApp(appPackage);
    await adminFirestore
      .collection('apps')
      .doc(serializedAppPackage.id as string)
      .set(serializedAppPackage);

    return true;
  } catch (error) {
    console.error('updateAnAppFromFirestore:', error);
    return null;
  }
};

export const addAppPackageToFirestore = async (appPackage: AppPackage) => {
  try {
    const serializedAppPackage = serializeApp(appPackage);

    await adminFirestore
      .collection('apps')
      .doc(serializedAppPackage.id as string)
      .set(serializedAppPackage);

    return true;
  } catch (error) {
    console.error('add app package to firestore error:', error);
    return false;
  }
};
