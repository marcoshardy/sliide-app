import { User, ApiErrorResp } from '@/types';
import type { NextApiRequest, NextApiResponse } from 'next';
import { getUserFromFirestore, verifyToken } from './utils/utils';

export default async (req: NextApiRequest, res: NextApiResponse<ApiErrorResp | User>) => {
  const tokenResp = await verifyToken(req, res);

  if (req.method === 'GET') {
    try {
      const uid = tokenResp as string;

      const user = await getUserFromFirestore(uid);

      if (user) {
        res.status(200).json(user);
      } else {
        throw new Error('Error fetching user from firestore');
      }
    } catch (error) {
      console.error('error:', error);
      res.status(500).json({ message: 'Error fetching user', code: 500 });
    }
  }
};
