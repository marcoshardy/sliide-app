import type { NextApiRequest, NextApiResponse } from 'next';
import { getAppsFromFirestore, verifyToken } from '@/pages/api/utils/utils';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  await verifyToken(req, res);

  if (req.method === 'GET') {
    try {
      const apps = await getAppsFromFirestore();

      if (apps) {
        res.status(200).json(apps);
      } else {
        throw new Error('Could not get apps from firestore');
      }
    } catch (error) {
      console.error('error:', error);
      res.status(500).json({
        message: 'Error fetching app packages. Please try again',
        code: 500,
      });
    }
  }
};
