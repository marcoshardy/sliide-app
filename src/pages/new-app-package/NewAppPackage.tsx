import { Breadcrumbs } from '@/components';
import { NewAppPackageForm } from '@/components/Forms';
import { Box } from '@mui/system';
import React from 'react';

export const NewAppPackage = () => {
  return (
    <Box>
      <Breadcrumbs />
      <NewAppPackageForm />
    </Box>
  );
};
