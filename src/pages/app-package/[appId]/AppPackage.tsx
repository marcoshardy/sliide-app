import React, { useState, useEffect } from 'react';
import { Actions, AppImgAndHeader, AppStatus, Breadcrumbs, InternalNotes, Loader } from '@/components';
import { AppPackage as AppPackageType } from '@/types/apps';
import { getAnApp, getApkDownloadUrl, updateAppPackage } from '@/utils/helpers';
import { Box, Typography, TextField, InputAdornment, Button, Divider, IconButton } from '@mui/material';
import { useRouter } from 'next/router';
import SaveAsIcon from '@mui/icons-material/SaveAs';
import EditIcon from '@mui/icons-material/Edit';
import CancelIcon from '@mui/icons-material/Cancel';
import { useAuthContext, useAppContext } from '@/context';
import { RelatedAppPackages } from '@/components/AppDirectory/RelatedAppPackages';
import { enqueueSnackbar } from 'notistack';

interface EditProps {
  isEditing: boolean;
  value: string;
}

export const AppPackage = () => {
  const [editAppName, setEditAppName] = useState<EditProps>({
    isEditing: false,
    value: '',
  });
  const [editBundleId, setEditBundleId] = useState<EditProps>({
    isEditing: false,
    value: '',
  });
  const [downloadUrl, setDownloadUrl] = useState<string | null>(null);
  const [app, setApp] = useState<AppPackageType>();
  const { user } = useAuthContext();
  const { isLoading, apps } = useAppContext();

  const router = useRouter();

  const fetchApp = async (appId: string) => {
    const resp = await getAnApp(appId);
    if (resp) {
      setApp(resp);
    }
  };

  useEffect(() => {
    const { appId } = router.query;
    if (appId && user && !isLoading) {
      fetchApp(appId as string);
      getApkDownloadUrl(appId as string).then((resp) => {
        if (resp) {
          setDownloadUrl(resp);
        }
      });
    }
  }, [user, isLoading]);

  if (!app) return null;

  const updateApp = async (type: 'appName' | 'bundleId') => {
    const updatedApp = { ...app };

    if (type === 'appName' && editAppName.value.length > 0) {
      updatedApp.appName = editAppName.value;
      setEditAppName({ isEditing: false, value: '' });
    }
    if (type === 'bundleId' && editBundleId.value.length > 0) {
      updatedApp.bundleId = editBundleId.value;
      setEditBundleId({ isEditing: false, value: '' });
    }

    const resp = await updateAppPackage(updatedApp);
    if (resp) {
      setApp(updatedApp);
      enqueueSnackbar('Successfully updated app package', {
        variant: 'success',
      });
    } else {
      enqueueSnackbar('Something went wrong', { variant: 'error' });
    }
  };

  return (
    <Box
      sx={{
        bgcolor: 'background.paper',
        px: '3rem',
      }}
    >
      {isLoading && <Loader />}
      <Breadcrumbs customPath={[`app-package-${app.id}`]} />

      <Box display="flex" flexDirection="column">
        <Box
          display="flex"
          flexDirection={{ xs: 'column', lg: 'row' }}
          alignItems={{ xs: 'flex-start', lg: 'flex-end' }}
        >
          <Box flex={1} display="flex" flexDirection="column" maxWidth="max-content">
            {editAppName.isEditing ? (
              <TextField
                variant="outlined"
                id="edit-appName-input"
                sx={{ mb: '1rem' }}
                value={editAppName.value}
                onChange={(e) => {
                  const text = e.target.value;
                  setEditAppName((oldS) => ({ ...oldS, value: text }));
                }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <IconButton
                        id="cancel-edit"
                        sx={{ mr: '1rem' }}
                        onClick={() => setEditAppName({ isEditing: false, value: '' })}
                      >
                        <CancelIcon color="error" />
                      </IconButton>
                      <IconButton id="confirm-edit" onClick={() => updateApp('appName')}>
                        <SaveAsIcon color="primary" />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            ) : (
              <Typography align="left" variant="h5" mt="1rem" mr="1rem">
                {app.appName}
                <IconButton
                  id="edit-appName-btn"
                  onClick={() => setEditAppName({ isEditing: true, value: app.appName })}
                  sx={{ pl: '1rem' }}
                  color="primary"
                >
                  <EditIcon />
                </IconButton>
              </Typography>
            )}
            {editBundleId.isEditing ? (
              <TextField
                variant="outlined"
                size="small"
                id="edit-bundleId-input"
                value={editBundleId.value}
                onChange={(e) => {
                  const text = e.target.value;
                  setEditBundleId((oldS) => ({ ...oldS, value: text }));
                }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <IconButton
                        sx={{ mr: '1rem' }}
                        id="cancel-edit"
                        onClick={() =>
                          setEditBundleId({
                            isEditing: false,
                            value: '',
                          })
                        }
                      >
                        <CancelIcon color="error" />
                      </IconButton>
                      <IconButton id="confirm-edit" onClick={() => updateApp('bundleId')}>
                        <SaveAsIcon color="primary" />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            ) : (
              <Typography align="left" variant="body1" color="gray">
                {app.bundleId}
                <IconButton
                  id="edit-bundleId-btn"
                  onClick={() => setEditBundleId({ isEditing: true, value: app.bundleId })}
                  sx={{ pl: '1rem' }}
                  color="primary"
                >
                  <EditIcon />
                </IconButton>
              </Typography>
            )}
          </Box>
          <Box display="flex" alignItems="center" justifyContent="flex-end" flex={1}>
            <Box>
              <AppStatus app={app} />
            </Box>
            <Box sx={{ ml: '2rem' }}>
              <Actions appId={app.id as string} />
            </Box>
          </Box>
        </Box>
        <Box display="flex" flexDirection={{ xs: 'column', lg: 'row' }}>
          <Box flex={1} mb="2rem">
            <Typography variant="h6" my="1rem">
              Changelog
            </Typography>
            <Typography variant="body2">- Fix issue with card</Typography>
            <Typography variant="body2">- Now includes sport category in main feed</Typography>
            <Typography variant="body2">- New App Icon design</Typography>
            <Typography variant="body2">- Category select screen added</Typography>
          </Box>

          <Box flex={1} mb="2rem" display="flex" flexDirection="column" justifyContent="flex-start">
            <AppImgAndHeader app={app} />

            <Box mt="1rem" display="flex" maxWidth="25rem" justifyContent="flex-start">
              <Button
                disabled={!downloadUrl}
                fullWidth
                id="download-apk"
                href={downloadUrl ?? '#'}
                variant="contained"
                sx={{ mr: '2rem' }}
              >
                Download
              </Button>
              <Button
                disabled={!downloadUrl}
                onClick={() => {
                  navigator.clipboard.writeText(downloadUrl as string);
                  enqueueSnackbar('Successfully copied to clipboard', {
                    variant: 'success',
                  });
                }}
                fullWidth
                variant="contained"
              >
                Copy Link
              </Button>
            </Box>
          </Box>
        </Box>
        <Box display="flex" flexDirection={{ xs: 'column', lg: 'row' }}>
          <InternalNotes app={app} />

          <Box flex={1} ml="1rem">
            <Box minHeight="3rem" display="flex" alignItems="center" justifyContent="space-between">
              <Typography variant="h5">Related App Packages</Typography>
            </Box>
            <Divider variant="fullWidth" sx={{ mb: '1rem' }} />
            <RelatedAppPackages apps={apps} />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
