import React, { useState } from 'react';
import { Breadcrumbs, FileDragDrop } from '@/components';
import { Box } from '@mui/system';
import { useRouter } from 'next/router';
import { enqueueSnackbar } from 'notistack';
import { getStorage, ref, uploadBytesResumable } from 'firebase/storage';
import type { UploadTaskSnapshot, UploadTask } from 'firebase/storage';
import { Typography, LinearProgress, LinearProgressProps, IconButton } from '@mui/material';
import PauseCircleFilledIcon from '@mui/icons-material/PauseCircleFilled';
import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import { useAppContext } from '@/context';

export const Upload = () => {
  const [uploadTaskSnapshot, setUploadTaskSnapshot] = useState<UploadTaskSnapshot>();
  const [uploadTaskStream, setUploadTaskStream] = useState<UploadTask>();
  const router = useRouter();

  const { apps } = useAppContext();

  const appId = router.query.appId;

  const app = apps.filter((a) => a.id === appId);

  const title = app[0] ? `Upload APK for ${app[0].appName}` : 'Upload your file';

  const uploadApk = async (apk: File) => {
    const storage = getStorage();

    const apkFileRef = ref(storage, `apps/${appId}/${appId}.apk`);

    // 'file' comes from the Blob or File API
    const stream = uploadBytesResumable(apkFileRef, apk);

    setUploadTaskStream(stream);

    stream.on(
      'state_changed',
      (snapshot) => {
        setUploadTaskSnapshot(snapshot);
      },
      (error) => {
        setUploadTaskSnapshot(undefined);
        setUploadTaskStream(undefined);
        // Handle unsuccessful uploads
        enqueueSnackbar(error.message.replace('Firebase Storage: ', ''), {
          variant: 'error',
        });
      },
      () => {
        setUploadTaskSnapshot(undefined);
        setUploadTaskStream(undefined);
        enqueueSnackbar('Successfully uploaded apk', { variant: 'success' });
      }
    );
  };

  function LinearProgressWithLabel(props: LinearProgressProps & { value: number }) {
    return (
      <Box sx={{ display: 'flex', alignItems: 'center', width: '70%' }}>
        <Box sx={{ width: '100%', mr: 1 }}>
          <Typography>Uploading...</Typography>

          <LinearProgress variant="buffer" {...props} />
        </Box>
        <Box sx={{ minWidth: 35 }}>
          <Typography variant="body2" color="text.secondary">{`${Math.round(props.value)}%`}</Typography>
        </Box>
      </Box>
    );
  }

  return (
    <Box height="calc(100vh - 9rem)">
      {appId && <Breadcrumbs customPath={['upload']} />}
      <Box height="100%" display="grid" gridTemplateRows="1fr 5fr">
        <Box display="flex" alignItems="center" justifyContent="center">
          {uploadTaskSnapshot ? (
            <Box width="100%" display="flex" alignItems="center" justifyContent="center">
              <LinearProgressWithLabel
                value={(uploadTaskSnapshot.bytesTransferred / uploadTaskSnapshot.totalBytes) * 100}
              />
              <Box>
                {uploadTaskSnapshot?.state === 'paused' && (
                  <IconButton color="primary">
                    <PlayCircleIcon
                      fontSize="large"
                      color="primary"
                      onClick={() => {
                        uploadTaskStream?.resume();
                      }}
                    />
                  </IconButton>
                )}
                {uploadTaskSnapshot?.state === 'running' && (
                  <>
                    <IconButton
                      color="primary"
                      onClick={() => {
                        uploadTaskStream?.pause();
                      }}
                    >
                      <PauseCircleFilledIcon fontSize="large" color="primary" />
                    </IconButton>
                    <IconButton
                      color="primary"
                      onClick={() => {
                        uploadTaskStream?.cancel();
                      }}
                    >
                      <CancelIcon fontSize="large" color="error" />
                    </IconButton>
                  </>
                )}
              </Box>
            </Box>
          ) : (
            <Typography>No Upload in progress</Typography>
          )}
        </Box>

        <FileDragDrop processFile={uploadApk} title={title} />
      </Box>
    </Box>
  );
};
