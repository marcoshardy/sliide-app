export interface User {
  createdAt: Date;
  updatedAt: Date;
  name: string;
  uid: string;
  email: string;
  firebaseToken: string;
}
