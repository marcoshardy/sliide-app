import type { Timestamp } from 'firebase/firestore';

export interface AppPackage {
  appImageUrl?: string;
  appName: string;
  bundleId: string;
  version: string;
  createdUid: string;
  createdAt: Date | Timestamp;
  notes: AppNote[];
  status: AppStatusEnum;
  id: string;
}

export enum AppStatusEnum {
  RELEASED = 'released',
  SUBMITTED = 'submitted',
  DRAFT = 'draft',
}

export interface AppNote {
  text: string;
  createdAt: Date;
  userId: string;
  userName: string;
  id: string;
}

export interface DialogState {
  title: string;
  open: boolean;
  onSuccess: () => void;
}

export const defaultDialogState = {
  title: '',
  open: false,
  onSuccess: () => {},
};
