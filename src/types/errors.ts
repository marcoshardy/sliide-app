export interface ApiErrorResp {
  message: string;
  code: number;
}
