import React, {
  useEffect,
  useState,
  createContext,
  useMemo,
  useContext,
} from 'react';
import { getAllApps } from '@/utils/helpers';
import { defaultDialogState, DialogState, AppPackage } from '@/types/apps';
import { useAuthContext } from '..';

interface AppContextType {
  setDialogState: (state: DialogState) => void;
  dialogState: DialogState;
  setIsLoading: (state: boolean) => void;
  isLoading: boolean;
  apps: AppPackage[];
}

const defaultState = {
  setDialogState: () => {},
  dialogState: defaultDialogState,
  isLoading: false,
  setIsLoading: () => {},
  apps: [],
};

interface AppProviderProps {
  children: React.ReactNode;
}

export const AppContext = createContext<AppContextType>(defaultState);

export const AppProvider: React.FC<AppProviderProps> = ({ children }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [dialogState, setDialogState] =
    useState<DialogState>(defaultDialogState);

  const { user } = useAuthContext();

  const [apps, setApps] = useState<AppPackage[]>([]);

  const fetchApps = async () => {
    const resp = await getAllApps();

    if (resp) {
      setApps(resp);
    }
  };

  useEffect(() => {
    if (user && !isLoading) {
      fetchApps();
    }
  }, [user, isLoading]);

  const memoState = useMemo(
    () => ({
      setDialogState,
      dialogState,
      isLoading,
      setIsLoading,
      apps,
    }),
    [dialogState, isLoading, apps]
  );

  return (
    <AppContext.Provider value={memoState}>{children}</AppContext.Provider>
  );
};

export const useAppContext = () => useContext(AppContext);
