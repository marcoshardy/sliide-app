import React, { useEffect, useState, createContext, useMemo, useContext } from 'react';
import { User } from '@/types/user';
import { useRouter } from 'next/router';
import { signInWithEmailAndPassword, signOut } from 'firebase/auth';
import { auth } from '@/config/firebaseClient';
import axios from 'axios';
import { enqueueSnackbar } from 'notistack';
import { getUserByToken, removeTokenFromLocalStorage, setTokenInLocalStorage } from '@/utils/helpers';

interface AppContextType {
  user: User | null;
  signUpUser: (name: string, email: string, password: string) => Promise<void>;
  loginUser: (email: string, password: string) => Promise<void>;
  logoutUser: () => Promise<void>;
  isLoading: boolean;
  setIsLoading: (state: boolean) => void;
}

const defaultState = {
  user: null,
  signUpUser: async () => {},
  loginUser: async () => {},
  logoutUser: async () => {},
  setIsLoading: () => {},
  isLoading: false,
};

interface AuthProviderProps {
  children: React.ReactNode;
}

export const AuthContext = createContext<AppContextType>(defaultState);

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [user, setUser] = useState<User | null>(null);

  const router = useRouter();

  const signUpUser = async (name: string, email: string, password: string) => {
    try {
      setIsLoading(true);
      const resp = await axios.post('/api/signup', {
        name,
        email,
        password,
      });
      const data = resp.data;

      if (data && 'name' in data && 'uid' in data && 'email' in data) {
        await signInWithEmailAndPassword(auth, email, password);
        initUser();
      }
      setIsLoading(false);
    } catch (error) {
      console.error('error:', error);
      setIsLoading(false);
    }
  };

  const loginUser = async (email: string, password: string) => {
    try {
      setIsLoading(true);
      await signInWithEmailAndPassword(auth, email, password);
      enqueueSnackbar('Successfully logged in', { variant: 'success' });
      initUser();
      setIsLoading(false);
    } catch (error) {
      console.error('error:', error);
      enqueueSnackbar('Error signing in. Please check your email or password and try again.', { variant: 'error' });
      setIsLoading(false);
    }
  };

  const initUser = async () => {
    try {
      const user = await getUserByToken();

      if (user) {
        setTokenInLocalStorage(user.firebaseToken);
        setUser(user);
        if (router.pathname === '/login' || router.pathname === '/sign-up' || router.pathname === '/') {
          router.push('/');
        }
      }
    } catch (error) {
      console.error('error:', error);
      enqueueSnackbar('Error signing in. Please check your email or password and try again.', { variant: 'error' });
    }
  };

  const logoutUser = async () => {
    try {
      await signOut(auth);
      removeTokenFromLocalStorage();
      setUser(null);
      router.push('/login');
    } catch (error) {
      console.error('error:', error);
      enqueueSnackbar('Error signing out.', { variant: 'error' });
    }
  };

  useEffect(() => {
    auth.onAuthStateChanged((firestoreUser) => {
      if (firestoreUser) {
        initUser();
      } else {
        if (router.pathname !== '/login' && router.pathname !== '/sign-up') {
          router.push('/login');
        }
      }
    });
  }, []);

  const memoState = useMemo(
    () => ({
      user,
      signUpUser,
      loginUser,
      logoutUser,
      setIsLoading,
      isLoading,
    }),
    [user, isLoading]
  );

  return <AuthContext.Provider value={memoState}>{children}</AuthContext.Provider>;
};

export const useAuthContext = () => useContext(AuthContext);
