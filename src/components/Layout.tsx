import React from 'react';
import { Navbar, Footer, Dialog } from '@/components';
import { VIEWPORT_HEIGHT } from '@/utils/helpers';
import '../app/globals.css';

export interface LayoutProps {
  children: React.ReactNode;
}

export default function Layout({ children }: LayoutProps) {
  return (
    <>
      <Navbar />
      <Dialog />
      <main style={{ minHeight: VIEWPORT_HEIGHT }}>{children}</main>
      <Footer />
    </>
  );
}
