import * as React from 'react';
import { Box, Dialog as MuiDialog, DialogTitle, Button } from '@mui/material';
import type { BoxProps } from '@mui/material';
import { defaultDialogState } from '@/types/apps';
import { useAppContext } from '@/context';

export const Dialog = () => {
  const { dialogState, setDialogState } = useAppContext();

  const { title, open, onSuccess } = dialogState;

  const onClose = () => {
    setDialogState(defaultDialogState);
  };
  return (
    <MuiDialog onClose={onClose} open={open}>
      <DialogTitle>{title}</DialogTitle>
      <Box {...DialogContainer}>
        <Button
          onClick={onClose}
          fullWidth
          id='dialog-cancel'
          size='medium'
          variant='outlined'
          sx={{ mr: '1rem' }}>
          Cancel
        </Button>
        <Button
          onClick={() => {
            onSuccess();
            onClose();
          }}
          fullWidth
          id='dialog-confirm'
          size='medium'
          variant='contained'>
          Confirm
        </Button>
      </Box>
    </MuiDialog>
  );
};

const DialogContainer: BoxProps = {
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-around',
  padding: '1rem',
};
