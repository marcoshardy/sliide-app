import * as React from 'react';
import { Box, Typography, Container, Link } from '@mui/material';
import type { BoxProps } from '@mui/material';

export const Footer = () => {
  return (
    <Box {...ContainerStyles}>
      <Container maxWidth='sm'>
        <Typography variant='body1'>
          My sticky footer can be found here.
        </Typography>
        <Typography variant='body2' color='text.secondary'>
          {'Copyright © '}
          <Link color='inherit' href='https://mui.com/'>
            Marcos Hardy
          </Link>{' '}
          {new Date().getFullYear()}
          {'.'}
        </Typography>
      </Container>
    </Box>
  );
};

const ContainerStyles: BoxProps = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  component: 'footer',
  height: '5rem',
  width: '100%',
  bgcolor: (theme) =>
    theme.palette.mode === 'light'
      ? theme.palette.grey[200]
      : theme.palette.grey[800],
};
