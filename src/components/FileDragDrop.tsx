import React, { useState } from 'react';
import { Paper, Typography, Button, Box } from '@mui/material';
import type { BoxProps } from '@mui/material';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { enqueueSnackbar } from 'notistack';

interface FileDropProps {
  processFile: (file: File) => void;
  title: string;
}

export const FileDragDrop: React.FC<FileDropProps> = ({ processFile, title }) => {
  const [isDragging, setIsDragging] = useState(false);
  const [file, setFile] = useState<File | null>(null);

  const isFileValid = (file: File) => {
    const maxFileSize = 10 * 10 * 1024 * 1024; // 100MB
    const allowedExtensions = ['apk'];

    if (file.size > maxFileSize) {
      enqueueSnackbar('File size exceeds the maximum limit of 100MB.', {
        variant: 'error',
      });
      return false;
    }

    const fileNameParts = file.name.split('.');
    const fileExtension = fileNameParts[fileNameParts.length - 1].toLowerCase();

    if (!allowedExtensions.includes(fileExtension)) {
      enqueueSnackbar('Invalid file format. Only APK files are allowed.', {
        variant: 'error',
      });
      return false;
    }

    return true;
  };

  const handleDragEnter = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setIsDragging(true);
  };

  const handleDragLeave = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setIsDragging(false);
  };

  const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setIsDragging(false);

    if (event.dataTransfer.files.length) {
      const inputFile = event.dataTransfer.files[0];

      if (isFileValid(inputFile)) {
        setFile(inputFile);
      }
    }
  };

  return (
    <Paper
      sx={{
        height: 'calc(100% - 10rem)',
        padding: '1rem',
        margin: '2rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
      }}
    >
      <Box flex={0.2} width="100%">
        <Typography variant="h6" align="left">
          {title}
        </Typography>
      </Box>
      <Box
        flex={0.8}
        width="100%"
        border={`1px dashed gray`}
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        {file ? (
          <Box
            display="flex"
            bgcolor={isDragging ? 'gray' : 'white'}
            alignItems="center"
            justifyContent="center"
            flexDirection="column"
          >
            <UploadFileIcon color="primary" fontSize="large" />
            <Typography variant="body1">{file.name}</Typography>
            <Typography variant="body1">{file.type}</Typography>
            <Box width="80%" display="flex" alignItems="center" justifyContent="space-around">
              <Button color="error" sx={{ mt: '1rem', mr: '1rem' }} onClick={() => setFile(null)}>
                Remove
              </Button>
              <Button
                id="confirm-file"
                color="primary"
                sx={{ mt: '1rem' }}
                onClick={() => {
                  processFile(file);
                  setFile(null);
                }}
              >
                Confirm
              </Button>
            </Box>
          </Box>
        ) : (
          <Box
            bgcolor={isDragging ? '#f8f8f8' : 'white'}
            onDragEnter={handleDragEnter}
            onDragLeave={handleDragLeave}
            onDragOver={(event) => event.preventDefault()}
            onDrop={handleDrop}
            {...DragDropContainer}
          >
            <UploadFileIcon color="primary" fontSize="large" />
            <Button
              id="add-file-btn"
              onClick={() => {
                const input = document.createElement('input');
                input.id = 'file-input';
                input.type = 'file';
                input.onchange = () => {
                  // you can use this method to get file and perform respective operations
                  if (input.files) {
                    const inputFile = Array.from(input.files)[0];

                    if (isFileValid(inputFile)) {
                      setFile(inputFile);
                    }
                  }
                };
                input.click();
              }}
            >
              Select a file
            </Button>
            <Typography variant="body1">Drag & Drop File here</Typography>
            <Typography variant="body1">Supports APK</Typography>
          </Box>
        )}
      </Box>
    </Paper>
  );
};

const DragDropContainer: BoxProps = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
  id: 'drop-container',
};
