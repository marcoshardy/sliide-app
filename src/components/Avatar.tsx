import * as React from 'react';
import { Avatar as MuiAvatar } from '@mui/material';
import PersonIcon from '@mui/icons-material/Person';

export const Avatar = () => {
  return (
    <MuiAvatar>
      <PersonIcon />
    </MuiAvatar>
  );
};
