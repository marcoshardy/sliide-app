import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import { Box, BoxProps } from '@mui/material';

export const Loader = () => {
  return (
    <Box sx={ContainerStyles}>
      <CircularProgress sx={{ zIndex: 10 }} />
    </Box>
  );
};

const ContainerStyles: BoxProps = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  position: 'absolute',
  top: '50%',
  left: '50%',
  zIndex: 4,
  bgcolor: 'rgba(25,25,25,0.3)',
  width: '100%',
  height: '100%',
};
