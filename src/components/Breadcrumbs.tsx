import React, { useState, useEffect } from 'react';
import {
  Breadcrumbs as MuiBreadcrumbs,
  Typography,
  Link,
  Box,
} from '@mui/material';
import type { BoxProps } from '@mui/material';
import { useRouter } from 'next/router';

interface BreadcrumbsProps {
  customPath?: string[];
}

export const Breadcrumbs: React.FC<BreadcrumbsProps> = ({ customPath }) => {
  const router = useRouter();
  const urlString = router.asPath;
  const rawPaths = urlString.split('/').slice(1);
  const [paths, setPaths] = useState<string[]>([]);

  useEffect(() => {
    if (!customPath) {
      setPaths(rawPaths);
    } else {
      setPaths(customPath);
    }
  }, []);
  return (
    <Box {...Container}>
      <MuiBreadcrumbs aria-label='breadcrumb'>
        <Link underline='hover' color='inherit' href='/'>
          Home
        </Link>
        {paths.length > 0 &&
          paths.map((path, i) => {
            return (
              <Link
                key={i}
                underline='hover'
                color='inherit'
                href={`${urlString.split(path)[0] + path}`}>
                <Typography textTransform='capitalize'>
                  {path.replaceAll('-', ' ')}
                </Typography>
              </Link>
            );
          })}
      </MuiBreadcrumbs>
    </Box>
  );
};

const Container: BoxProps = {
  width: '100vw',
  paddingY: '1rem',
  marginLeft: '1rem',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-start',
};
