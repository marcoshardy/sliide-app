import React, { useState } from 'react';
import { Avatar } from '@/components';
import { AppNote, AppPackage, AppPackage as AppPackageType } from '@/types/apps';
import { randomId, updateAppPackage } from '@/utils/helpers';
import { Box, Typography, TextField, Stack, Divider, IconButton } from '@mui/material';
import type { BoxProps } from '@mui/material';
import { enqueueSnackbar } from 'notistack';

import { useAppContext } from '@/context';
import SaveAsIcon from '@mui/icons-material/SaveAs';
import { auth } from '@/config/firebaseClient';
import { validateAppPackage } from '@/utils/forms/forms';
import moment from 'moment';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import EditIcon from '@mui/icons-material/Edit';
import CancelIcon from '@mui/icons-material/Cancel';

interface EditState {
  isEditing: boolean;
  note?: AppNote;
}

interface InternalNotesProps {
  app: AppPackage;
}

export const InternalNotes: React.FC<InternalNotesProps> = ({ app }) => {
  const [note, setNote] = useState<string>('');
  const [editState, setEditState] = useState<EditState>({ isEditing: false });

  const { setIsLoading, setDialogState } = useAppContext();

  const currUserId = auth.currentUser?.uid as string;

  const handleSubmit = async () => {
    const newNote: AppNote = {
      createdAt: new Date(),
      userId: currUserId,
      userName: auth.currentUser?.displayName as string,
      text: note,
      id: randomId(10),
    };

    const updatedAppPackage: AppPackageType = { ...app };

    if (editState.isEditing) {
      newNote.id = editState.note?.id as string;

      updatedAppPackage.notes = updatedAppPackage?.notes.map((n) => {
        if (n.id === newNote.id) {
          n.text = note;
        }
        return n;
      });
    } else {
      if (updatedAppPackage.notes) {
        updatedAppPackage.notes.push(newNote);
      } else {
        updatedAppPackage.notes = [newNote];
      }
    }

    setIsLoading(true);
    const resp = await updateAppPackage(updatedAppPackage);

    if (resp) {
      enqueueSnackbar('Successfully added note', {
        variant: 'success',
      });
      setNote('');
      setEditState({ isEditing: false });
    } else {
      enqueueSnackbar('Something went wrong. Please try again', {
        variant: 'error',
      });
    }
    setIsLoading(false);
  };

  const deleteNote = (noteId: string) => {
    const deleteCommentDialogState = {
      title: 'Are you sure you want to delete this note?',
      open: true,
      onSuccess: () => {
        removeCommentHelper(noteId);
      },
    };

    setDialogState(deleteCommentDialogState);
  };

  const editNote = (note: AppNote) => {
    setEditState({ isEditing: true, note });
    setNote(note.text);
  };
  const cancelEdit = () => {
    setEditState({ isEditing: false });
    setNote('');
  };

  const removeCommentHelper = async (noteId: string) => {
    const updatedAppPackage: AppPackageType = { ...app };

    const currNotes = updatedAppPackage.notes as AppNote[];

    const updatedNotes = currNotes.filter((nt) => nt.id !== noteId);

    updatedAppPackage.notes = updatedNotes;

    const validationResp = validateAppPackage(updatedAppPackage, true);

    if (validationResp.isValid) {
      setIsLoading(true);
      const resp = await updateAppPackage(updatedAppPackage);

      if (resp) {
        enqueueSnackbar('Successfully updated app package', {
          variant: 'success',
        });
      } else {
        enqueueSnackbar('Something went wrong. Please try again', {
          variant: 'error',
        });
      }
      setIsLoading(false);
    } else {
      enqueueSnackbar(validationResp.errorMsg, {
        variant: 'error',
      });
    }
  };

  return (
    <Box flex={1}>
      <Box minHeight="3rem" display="flex" alignItems="center" justifyContent="space-between">
        <Typography variant="h5">Internal Notes</Typography>
        {note.length > 0 && (
          <IconButton id="save-note" onClick={handleSubmit} color="info">
            <SaveAsIcon />
          </IconButton>
        )}
        {note.length > 0 && editState.isEditing && (
          <IconButton id="cancel-note" onClick={cancelEdit} color="info">
            <CancelIcon />
          </IconButton>
        )}
      </Box>
      <Divider variant="fullWidth" sx={{ mb: '1rem' }} />
      <Stack>
        <Box display="flex" alignItems="center">
          <Avatar />
          <TextField
            sx={{ ml: '1rem', mb: '1rem' }}
            multiline
            id="add-note-input"
            label={editState.isEditing ? 'Edit comment...' : 'Add a comment...'}
            variant="outlined"
            fullWidth
            onChange={(e) => {
              setNote(e.target.value);
            }}
            value={note}
          />
        </Box>
      </Stack>
      {app?.notes && app.notes.length > 0 ? (
        <Box display="flex" flexDirection="column" alignItems="flex-start" justifyContent="flex-start">
          {app.notes
            .slice(0)
            .reverse()
            .map((note, i) => {
              return (
                <Box key={i} display="flex" alignItems="center">
                  <Avatar />
                  <Box ml="1rem" display="flex" flexDirection="column" width="100%">
                    <Box {...UserNameContainer}>
                      <Typography variant="body1">{note.userName}</Typography>
                      <Typography variant="body2" ml="1rem" color="gray">
                        ~{moment(new Date(note.createdAt)).fromNow()}
                      </Typography>

                      {note.userId === currUserId && (
                        <Box position="absolute" top={0} right="1rem">
                          <IconButton id="delete-note" onClick={() => deleteNote(note.id)} color="error">
                            <RemoveCircleIcon />
                          </IconButton>
                          <IconButton id="edit-note" onClick={() => editNote(note)} color="primary">
                            <EditIcon />
                          </IconButton>
                        </Box>
                      )}
                    </Box>
                    <Box mt="0.5rem">
                      <Typography variant="subtitle1">{note.text}</Typography>
                    </Box>
                  </Box>
                </Box>
              );
            })}
        </Box>
      ) : (
        <Box height="50%" display="flex" alignItems="center" justifyContent="center">
          <Typography>No notes for this app package</Typography>
        </Box>
      )}
    </Box>
  );
};

const UserNameContainer: BoxProps = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-start',
  minWidth: '100%',
  position: 'relative',
};
