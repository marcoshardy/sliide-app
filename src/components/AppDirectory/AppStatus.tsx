import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Menu, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { AppPackage, AppStatusEnum } from '@/types/apps';
import { useAppContext } from '@/context';
import { updateAppPackage } from '@/utils/helpers';
import { enqueueSnackbar } from 'notistack';

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === 'light'
        ? 'rgb(55, 65, 81)'
        : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

interface ActionsProps {
  app: AppPackage;
  readOnly?: boolean;
}

type MuiColor =
  | 'inherit'
  | 'primary'
  | 'secondary'
  | 'success'
  | 'error'
  | 'info'
  | 'warning'
  | undefined;

export const AppStatus: React.FC<ActionsProps> = ({
  app,
  readOnly = false,
}) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const { setDialogState, setIsLoading } = useAppContext();

  const updateStatusHelper = async (status: AppStatusEnum) => {
    setIsLoading(true);

    const updatedAppPackage: AppPackage = { ...app, status };
    const resp = await updateAppPackage(updatedAppPackage);

    if (resp) {
      enqueueSnackbar('Successfully updated app status', {
        variant: 'success',
      });
    } else {
      enqueueSnackbar('Something went wrong. Please try again', {
        variant: 'error',
      });
    }
    setIsLoading(false);
  };

  const handleClick = (status: AppStatusEnum) => {
    const deleteAppDialogState = {
      title: `Are you sure you want to update the status of this app to: ${status.toUpperCase()}`,
      open: true,
      onSuccess: () => {
        updateStatusHelper(status);
      },
    };
    setDialogState(deleteAppDialogState);
    handleClose();
  };

  const handleOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const getColor = (): MuiColor => {
    if (app.status === AppStatusEnum.RELEASED) {
      return 'success';
    }
    if (app.status === AppStatusEnum.SUBMITTED) {
      return 'warning';
    }
    if (app.status === AppStatusEnum.DRAFT) {
      return 'primary';
    }
  };

  return (
    <div>
      <Button
        id='app-status-btn'
        aria-controls={open ? 'demo-customized-menu' : undefined}
        aria-haspopup='true'
        aria-expanded={open ? 'true' : undefined}
        variant='outlined'
        color={getColor()}
        disableElevation
        onClick={handleOpen}
        sx={{ width: '10rem' }}
        endIcon={!readOnly && <KeyboardArrowDownIcon />}>
        {app.status}
      </Button>
      <StyledMenu
        id='demo-customized-menu'
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}>
        <MenuItem
          id='submitted'
          onClick={() => handleClick(AppStatusEnum.SUBMITTED)}>
          Submitted
        </MenuItem>
        <MenuItem
          id='released'
          onClick={() => handleClick(AppStatusEnum.RELEASED)}>
          Released
        </MenuItem>
        <MenuItem id='draft' onClick={() => handleClick(AppStatusEnum.DRAFT)}>
          Draft
        </MenuItem>
      </StyledMenu>
    </div>
  );
};
