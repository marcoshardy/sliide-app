import * as React from 'react';
import { Typography, Divider, Box } from '@mui/material';
import type { BoxProps } from '@mui/material';
import { AppStatus } from '@/components';
import { AppPackage } from '@/types/apps';

interface RelatedAppPackagesProps {
  apps: AppPackage[];
}

export const RelatedAppPackages: React.FC<RelatedAppPackagesProps> = ({
  apps,
}) => {
  return (
    <Box
      display='flex'
      alignItems='center'
      justifyContent='center'
      flexDirection='column'>
      {apps.map((app, i) => {
        return (
          <Box key={i} width='100%' mb='1rem'>
            <Box key={i} {...AppStatusContainer}>
              <Box flex={1} display='flex' alignItems='center'>
                <Typography variant='h6'>v{app.version}</Typography>
              </Box>
              <Box
                flex={1}
                display='flex'
                alignItems='center'
                justifyContent='flex-end'>
                <AppStatus readOnly app={app} />
              </Box>
            </Box>
            <Divider
              variant='fullWidth'
              sx={{ mt: '1rem', borderStyle: 'dashed' }}
            />
          </Box>
        );
      })}
    </Box>
  );
};

const AppStatusContainer: BoxProps = {
  width: '80%',
  margin: '0 auto',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
};
