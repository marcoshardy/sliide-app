import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Box,
  Typography,
} from '@mui/material';
import type { BoxProps } from '@mui/material';
import { Actions, AppImgAndHeader, AppStatus } from '@/components';
import { AppPackage } from '@/types/apps';

interface AppDirectoryTableProps {
  apps: AppPackage[];
}

export const AppDirectoryTable: React.FC<AppDirectoryTableProps> = ({
  apps,
}) => {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 750 }} aria-label='simple table'>
        <TableHead>
          <TableRow>
            <TableCell align='left'>App</TableCell>
            <TableCell align='center'>Version</TableCell>
            <TableCell align='center'>Notes</TableCell>
            <TableCell align='center'>Status</TableCell>
            <TableCell align='right' />
          </TableRow>
        </TableHead>
        {apps.length > 0 ? (
          <TableBody>
            {apps.map((app) => (
              <TableRow key={app.id}>
                <TableCell
                  width={'35%'}
                  align='center'
                  component='th'
                  scope='row'>
                  <AppImgAndHeader app={app} />
                </TableCell>
                <TableCell align='center'>v{app.version}</TableCell>
                <TableCell align='center'>{app?.notes?.length || 0}</TableCell>
                <TableCell align='center'>
                  <AppStatus app={app} />
                </TableCell>
                <TableCell align='center'>
                  <Actions appId={app.id as string} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        ) : (
          <Box {...NoResultsContainer}>
            <Typography>No Results</Typography>
          </Box>
        )}
      </Table>
    </TableContainer>
  );
};

const NoResultsContainer: BoxProps = {
  width: '270%',
  padding: '2rem',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
};
