import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Menu, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import VisibilityIcon from '@mui/icons-material/Visibility';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { useRouter } from 'next/router';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import { useAppContext } from '@/context';
import { deleteAnApp } from '@/utils/helpers';
import { enqueueSnackbar } from 'notistack';

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === 'light'
        ? 'rgb(55, 65, 81)'
        : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

interface ActionsProps {
  appId: string;
}

export const Actions: React.FC<ActionsProps> = ({ appId }) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const { setDialogState, setIsLoading } = useAppContext();

  const router = useRouter();

  const deleteAppDialogState = {
    title: 'Are you sure you want to delete this App Package?',
    open: true,
    onSuccess: () => {
      setAnchorEl(null);
      deleteAnAppHelper();
    },
  };

  const deleteAnAppHelper = async () => {
    setIsLoading(true);
    const hasDeleted = await deleteAnApp(appId);
    if (hasDeleted) {
      setIsLoading(false);
      enqueueSnackbar(`Successfully deleted app ${appId}`, {
        variant: 'success',
      });
      if (router.pathname === '/') router.push('/');
    } else {
      setIsLoading(false);

      enqueueSnackbar('Something went wrong. Please try again', {
        variant: 'error',
      });
    }
  };

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleView = () => {
    router.push(`/app-package/${appId}`);
  };
  const handleVPublish = () => {
    router.push(`/app-package/${appId}/upload`);
  };
  const handleDelete = () => {
    setDialogState(deleteAppDialogState);
  };

  return (
    <div>
      <Button
        id='action-btn'
        aria-controls={open ? 'demo-customized-menu' : undefined}
        aria-haspopup='true'
        aria-expanded={open ? 'true' : undefined}
        variant='outlined'
        disableElevation
        onClick={handleClick}
        sx={{ width: '10rem' }}
        endIcon={<KeyboardArrowDownIcon />}>
        Actions
      </Button>
      <StyledMenu
        id='demo-customized-menu'
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}>
        <MenuItem id='view' onClick={handleView} disableRipple>
          <VisibilityIcon />
          View
        </MenuItem>
        <MenuItem id='upload-apk' onClick={handleVPublish} disableRipple>
          <FileUploadIcon />
          Upload APK
        </MenuItem>
        <MenuItem id='delete-app' onClick={handleDelete} disableRipple>
          <DeleteForeverIcon />
          Delete
        </MenuItem>
      </StyledMenu>
    </div>
  );
};
