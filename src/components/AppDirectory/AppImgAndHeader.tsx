import React from 'react';
import { Box, Typography } from '@mui/material';
import type { BoxProps } from '@mui/material';
import { AppPackage } from '@/types/apps';
import Image from 'next/image';

interface AppImgAndHeaderProps {
  app: AppPackage;
}

export const AppImgAndHeader: React.FC<AppImgAndHeaderProps> = ({ app }) => {
  return (
    <Box
      height='5rem'
      display='flex'
      alignItems='center'
      justifyContent='flex-start'>
      <Box display='flex' alignItems='center' justifyContent='flex-start'>
        <Box {...ImgContainer}>
          {app.appImageUrl && app.appImageUrl.length > 0 && (
            <Image
              src={app.appImageUrl}
              alt='app logo'
              width={80}
              height={65}
            />
          )}
        </Box>
      </Box>
      <Box
        display='flex'
        flexDirection='column'
        alignItems='flex-start'
        justifyContent='center'>
        <Typography align='left'>{app.appName}</Typography>
        <Typography align='left' color='gray'>
          {app.bundleId}
        </Typography>
      </Box>
    </Box>
  );
};

const ImgContainer: BoxProps = {
  borderRadius: 2,
  height: '4rem',
  minWidth: '5rem',
  marginRight: '1rem',
  bgcolor: 'yellow',
  overflow: 'hidden',
};
