import React, { useState } from 'react';
import {
  Avatar,
  Button,
  CssBaseline,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  Container,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';

import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { createNewAppPackage } from '@/utils/forms/forms';
import { AppPackage, AppStatusEnum } from '@/types/apps';
import { enqueueSnackbar } from 'notistack';
import { auth } from '@/config/firebaseClient';
import { useRouter } from 'next/router';
import { randomId } from '@/utils/helpers';

const theme = createTheme();

export const NewAppPackageForm = () => {
  const [status, setStatus] = useState<AppStatusEnum>(AppStatusEnum.DRAFT);

  const router = useRouter();

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const appName = data.get('appName') as string;
    const bundleId = data.get('bundleId') as string;
    const appImageUrl = data.get('appImageUrl') as string;
    const version = data.get('version') as string;
    const status = data.get('status') as AppStatusEnum;

    if (!appName || appName.length < 2) {
      enqueueSnackbar('App Name must exist and be longer than 2 characters', {
        variant: 'error',
      });
      return;
    }

    if (!bundleId || bundleId.length < 5) {
      enqueueSnackbar(
        'App Bundle ID must exist and be longer than 5 characters',
        { variant: 'error' }
      );
      return;
    }

    if (!version || version.length < 2) {
      enqueueSnackbar(
        'App Version must exist and be longer than 2 characters',
        { variant: 'error' }
      );
      return;
    }

    const newAppPackage: AppPackage = {
      appName,
      bundleId,
      status,
      version: version,
      appImageUrl,
      notes: [],
      createdAt: new Date(),
      createdUid: auth.currentUser?.uid as string,
      id: randomId(10),
    };

    const resp = await createNewAppPackage(newAppPackage);
    if (resp) {
      const createdAppPackage: AppPackage = { ...resp };
      router.replace(`/app-package/${createdAppPackage?.id as string}`);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}>
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component='h1' variant='h5'>
            New App Package
          </Typography>
          <Box
            component='form'
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  autoComplete='given-name'
                  name='appName'
                  required
                  fullWidth
                  id='appName'
                  label='App Name'
                  type='text'
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id='bundleId'
                  label='Bundle ID'
                  type='text'
                  name='bundleId'
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  name='appImageUrl'
                  label='App Image URL'
                  type='text'
                  id='appImageUrl'
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name='version'
                  label='Version'
                  type='text'
                  id='version'
                />
              </Grid>
              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel>Status</InputLabel>
                  <Select
                    id='status'
                    name='status'
                    value={status}
                    label='Status'
                    onChange={(e) => {
                      setStatus(e.target.value as AppStatusEnum);
                    }}>
                    <MenuItem value={AppStatusEnum.DRAFT}>Draft</MenuItem>
                    <MenuItem value={AppStatusEnum.SUBMITTED}>
                      Submitted
                    </MenuItem>
                    <MenuItem value={AppStatusEnum.RELEASED}>Released</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{ mt: 3, mb: 2 }}>
              Create App Package
            </Button>
          </Box>
        </Box>
        <Typography variant='body2' color='text.secondary' align='center'>
          {'Copyright © '}
          <Link color='inherit' href='https://mui.com/'>
            Marcos Hardy
          </Link>{' '}
          {new Date().getFullYear()}
          {'.'}
        </Typography>
      </Container>
    </ThemeProvider>
  );
};
