import { test, expect } from '@playwright/test';
import path from 'path';
import { loginUser, sleep } from './test-utils';

const imgUrl =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAllBMVEUAqvAAw//////y8vIAt/gAwv8AwP8Ap/AAqPDw+v/5+fkApfAtx//59fLk7PIrr/CB2P/E7//3/v8ArfHa9f+p5v+B3P+f5P9g1P+16v9Bz/8vy/8tufR4y/ai2Pmu4vtav/SJ0/jV7vzl9v6W2PlFvfTL5fLM8f/f7PK13fFpwvJy2P+q2fGR4P/F4vGd1PGDyvJ9yfHXxYyfAAALeElEQVR4nN3da3uiOhAA4IBCsIgexVrrZau1VWtvu///zx1QuSaDCWZGcT53n8d3AxkyDAmzFWK4HjyOJ9NZ17o82uzyCMP2fPHnZT1U+fHsrG70NOm6rutEYQBoRBiHx7nvLVevnQuF23HXcc3QDAuPTM8P94P6wuHTzNDAYQnj4DxcVQ0kLOyMo9EzzMMQHpCLtbawM7Zc8zwkYXxXgkZA+GghDB+iMDKyNw3haIpxfeIKGfPbr6rCJ+PTC4kwMq6UhMMJzg1IIGT+uzirCsJRF3EAsYWMh9tzwgHmFYovjIwv1cJnvCmGSMj8lyrhI+otSCNk/hssfMYHEghLo5gXDgiAFMIiMSccYU8yZELGtzLhEDlNUApZ2JEIJyRAIiF/F4VPFDchmTD3AJcIRzQjSCZk/mtJOL03IWsXhQSpnlqYJP6jsIO24L2ekLF1TjgmG0JCobfIhB0yH+kY8nUqJBxCSuFxEGPhkOwmpBUy3jkJqZI9vXB1Es7udQxZeBRuKYG0Qj44CMd3LNzHQqJV01WE8SqK0T1zX0MYPX8z2pmUWuitIiHRyvdKwqXNiG9DYiHzhmxNe5FSC/01IykhXk/IXxjd2vc6wj+MNt/T34cLRjyVUgvZnJGVoK4kbDPShcUVhCEz0at220J0kuO4cSSXSiLk3I+CExixfe7sezBajx4/XTcn5Ozvx67X2/y++16zhe4066v7PhRlY6EXfgVB0IoiCDZL7HHEBX7nXlXa63hWa8cvhnoH3jGCX+RRRAU+2YUYRqmpzfiyVYjgC3cUMYFjuxSdaAy9fqsUwS8qEQ/ozMrAuF2n7W0CgThvptB9FoX2JPwnAFvBD+Yg4gktCdBet3sCMCKGTRQ6nzKhPRKHMBL+RRxENGF5Ij3FgwSIO9fgCWW3ISBsbfx7F/40UviocZV+NPEqBWYaubCRM43T1RBipnw0oeUKDcmgcNPMjO9MVIXBHnN5gSe0HNk3VzLhBnX9hCmUPHrLhMGyqUJpwhCFwQdiMkQWWpb4ladkDDEfu7GFjrAGFoXBCrlQgyq0nNFZ4Q7Xhy6cnhMG+0bX2qLJppwxSkLc5T2FUMgYZeF7wyvC4kK4KAy+cDMFhdByh1VjiO4jEJZWUQUh6qqJTFjKGAUheqYgEk4gIfIDKZ1wCwgJMgWJsLRMzAtRi/l0QrcDCINf/ExBISy+QiyMIcVNiC90ukNAGPwjuQvxn0vLdeFUiFu6IBPCawuaTEEghNaH2KULKqGk7p0I++kvwB5MXKG4EcfDKVMk0wz/12pwFUP2CvEo3KXjFvaQGxWuUy/NShf8N2i1+pW/8IaF0jeID8VMMW8hv1tDfW8hZIpEmJUu/J8AvZSBJ3Slu2895EsX3vLYt9DMd0+SanAyhlmRe3fKHYsmviGVVPSPwqzIzf8mrSc9PCCaEHiNHwmz0kXW4YaZMZCA0kxxEOYyxVeuewhvNYwkFGrdqTArXczzXaZ4GQMHKH/DfYh0sPxNvuyGt9bAEbrgfptp6YLvix1uO6xBxAHKM0UUnexrhF0BiLfmxwAKpYssPtOvEd7KTYo9pHfBGEIoU8RfxidCoRcard8bASh/ID3ENPmihH9I+kznKJMNglDeDBXHs3sSeu8SIFIN3DywIlNYTvLNzEYEYmUMBCGYKb7d01dBXNLOjpYxjAPLRe4s1slXQawva2dvIXWemAZWZIpJ+t3TUj6ELZSChmkh0Pwcxfbw8drxKv0BiBgZwzCwIlMcv1Y9zjRzaAwRChqGha5Q5E7iMf/9of8LXafmu/bNAoHm7iiGp79InmnAQTTeI2VWaIGZIvnuP3mmAfIFQkHDqA/4TsbO7TyZjKEnzfkthIKGSaAzq84UBSGcMQwXNEwK4UyR7S+Srg+lz96HQTRc0DAIrMgU2SY42QpYXD8lRLMZw6BQXuSOI7dTUybkYMYw+3hqDggUue3izpP5PRWAp1PDJXBzQDhTfOb23sgJ+QLMGCYLGsaEcOmisF9afgxpMoYpIFjkLu1RXBDKVvrHMJgxTAnBIndpt/7C3iYkGcMQsLp0AQlZCA2iwYKGKSGYKb6LW20VhXyFnzHMACtKF6W/LO/AswOE5hqkdS2ONCqK3G7hD6128eorv73Iohcauk61dNEF15WFBT6Qjsr/YN4/REaECxphXxYh43p0ndGbPKsdOVgZwSHSpbw3BycbebR2H3tPx6gMdCdggUInkr62dCnvf4FJEaRrbUykDATnklrC3Nv8UBN4QGoUVlWB4Oq9pjB4EzsydIjqTeJqQLjCVFeYL/5CGaOSqFyxUhzDs8d9aguz4q8HZoyqUK5YqV2jhm5Cu/A1QrqUBzNG5SCq3opqFyl8CGZ9Ya6bDcwYVbFTTBlKQHhldIkwW8rDJfCqUFxhKQnhhcMlwtytFEIFjYpQXX4oCY3NpKVv13Ld3vqDGCxuX5jPGFBBo0KouF/INa/S3FcXFSVwUGjyKoUXR5cJ8/3QUEEDDsX3xSpCy31FEuaW8mBBAwrV1mklocEbsfw9frqUh0vg8lCdaBSf2uBXuxcK88VfvYyhXMhRE5rL+cK+GFnG0Hs8Ve7aUBOam07F3VvSRxN5oxQQ6mtgRaHlTM0sL8QdeNL99ipK4OX/lZ7GIl9VaDkW+GLiMmGW19QyRhAEvzp1OGVhXGn7fN52Lo2eEJv0x8AFjfSPdz9fC6ZVSlUXHpAXR+gLkf1euKCxyf25Zh1VS2ggzuw6j1ACvy1hRcao3dN3W0KMnr4bE8IFjdq7aNyaEC6B1+3puzUhnDHq9vTV+ZXyN2yHl2eXC+GMUXNHojrAyX9AKBzLo3BGCZgx3moNYg0gvM74NCE03QWuL4S7LlTO/1IYQx/MGLU209AGnu3kvljI+mDGqNPTpy0828l9uRAuaNTJGLpAeCmsdri30mlIYAm8TpuNrhC+C9VOcFMSgiXwOr1SukILeg2leJ6p2olWYE9fjZyo6YNbuRUPcFMUggUN/YShK4RmUtWTIhVPJQMLGvrP36aEqseZqp67BnSB11hh6AqBVxjKZ+6qCsUvoQ/RQ79KgRfenfP/UFMIPJ7izzRAY43CA6muUFrQqNMerSuU3ogj9eNa1cdQVgKvs9DXFUo/jNE4CFPjhEexoFGrWKMtlHQPjTVO3NUQimvhWt+16QOdWYmodaSwzimdvFizCXr9Ok21+sKIWFhe6J0orHUOaSFlBJt6ey7UEFqO850O43aqdyi03kmrfL45nnYZBLu/Nbui6wjjFxiTx8F69PzddTVP29U9S9bvr742u4/fPaeuCJ9OiNU+TVj/tNz41Fmu+zomF/d/HvD9n+l8/+dy3//Z6nrZrHFCb8G0HkiaJ+R/mGr1oanCF7a+b6G/ZkPVAkszhd6Q2cSTKa3QW9pMvYbUSOEqEirWqpsp9F8jIfGNSCsMO5FQcwXbKCHf27Fwe8fCwUGoUylrmDC0j0LS2ZRSyFcn4fBex5B3TkKtemeDhN7CToTq71UaJeTrVEg5iHTC4xCehGptFA0TsnVOqFeYb4bQf7PzQtU+gwYJ23ZRSPb8TSWMnrmLQrK0TyT0V3ZZSLUSphHyd1sUEq2iaIRhRyKMbkUKIomQ5/ZMzwmV25puXui/2HJhaWe1xgoLwKKQIvHjC5NULxVGo4h9L6ILiyMoCO0B9nSDLeQloCC0R8hJA1fIQ+HkCUFoDyeoNyOq0H8XP1YWhdEDHOaViinMHtXOCO3RFG/CwRP67VcZRiqM0gbakhhL6LE3OQUQ2p2xhXM74gg9voC+IYCEsdHBuFYxhBz2VQmjWfVppvRJ4XWFnIerqu0eqoRRbMddp0ZrF5XQ8/xwD37EoySMBnL0NOkeOtjMQE0JPc59b7l6Pbtbx1nhQbkePI4n05mJHjgTwjBszxd/XtQ2U/0fIfQd9HUHpLAAAAAASUVORK5CYII=';

const createAppData = [
  {
    data: {
      appName: '',
      bundleId: 'com.test.test',
      appImageUrl: imgUrl,
      version: '1.0.0',
      status: 'draft',
    },
    errorMsg: 'App Name must exist and be longer than 2 characters',
  },
  {
    data: {
      appName: 'Test App',
      bundleId: '',
      appImageUrl: imgUrl,
      version: '1.0.0',
      status: 'draft',
    },
    errorMsg: 'App Bundle ID must exist and be longer than 5 characters',
  },
  {
    data: {
      appName: 'Test App',
      bundleId: 'com.test.test',
      appImageUrl: imgUrl,
      version: '',
      status: 'draft',
    },

    errorMsg: 'App Version must exist and be longer than 2 characters',
  },
];

test.beforeEach(async ({ page }) => {
  await loginUser(page);
});

test('Invalid new app package data', async ({ page }) => {
  for (const { data, errorMsg } of createAppData) {
    await page.goto('http://localhost:3000/');
    await page.getByText('Add Package').click();

    await page.getByLabel('App Name').fill(data.appName);
    await page.getByLabel('Bundle ID').fill(data.bundleId);
    await page.getByLabel('App Image URL').fill(data.appImageUrl);
    await page.getByLabel('Version').fill(data.version);

    await page.click('#status');
    await page.click(`li[data-value='${data.status}']`);

    await page.getByText('Create App Package').click();
    const errorAlert = page.getByText(errorMsg);

    expect(errorAlert).toBeTruthy();
  }
});

test('Create app package', async ({ page }) => {
  await page.goto('http://localhost:3000/');
  await page.getByText('Add Package').click();

  await page.getByLabel('App Name').fill('Test App');
  await page.getByLabel('Bundle ID').fill('com.testdomain.tesapp');
  await page.getByLabel('App Image URL').fill(imgUrl);
  await page.getByLabel('Version').fill('1.0.0');

  await page.click('#status');
  await page.click(`li[data-value='draft']`);

  await page.getByText('Create App Package').click();

  const successAlert = page.getByTitle('Successfully created new app package');

  expect(successAlert).toBeTruthy();

  await expect(page).toHaveURL(/.*app-package/);
});

test('Create / Update / Delete app package notes', async ({ page }) => {
  await page.goto('http://localhost:3000/');
  await page.click('#action-btn');
  await page.click('#view');

  await expect(page).toHaveURL(/.*app-package/);

  //Create Note
  await page.fill('#add-note-input', 'This is a test note');
  await page.click('#save-note');
  expect(page.getByText('This is a test note')).toBeTruthy();

  //Edit Note
  await page.click('#edit-note');
  await page.fill('#add-note-input', 'This is an editted test note');
  await page.click('#save-note');
  expect(page.getByText('This is an editted test note')).toBeTruthy();

  //Delete Note
  await page.click('#delete-note');
  await page.click('#dialog-confirm');
  await page.waitForLoadState('networkidle');
  expect(await page.getByTitle('This is an editted test note').count()).toEqual(0);
});

test('Update app package', async ({ page }) => {
  await page.goto('http://localhost:3000/');
  await page.click('#action-btn');
  await page.click('#view');

  await expect(page).toHaveURL(/.*app-package/);

  //Update App Name
  await page.click('#edit-appName-btn');
  await page.fill('#edit-appName-input', 'Updated Test App');
  await page.click('#confirm-edit');
  expect(page.getByText('Updated Test App')).toBeTruthy();

  //Update App Bundle ID
  await page.click('#edit-bundleId-btn');
  await page.fill('#edit-bundleId-input', 'com.updated.bundleId');
  await page.click('#confirm-edit');
  expect(page.getByText('com.updated.bundleId')).toBeTruthy();

  //Update App Status
  await page.click('#app-status-btn');
  await page.click('#submitted');
  await page.click('#dialog-confirm');

  expect(page.getByText('Released')).toBeTruthy();
});

test('Upload apk to app and check download link', async ({ page, browserName }) => {
  if (browserName === 'firefox') {
    expect(true).toBeTruthy();
    return;
    // current bug with firefox where it does not receive responses when a payload is greater then 1MB
    // https://github.com/microsoft/playwright/issues/22753
  }

  await page.goto('http://localhost:3000/');
  await page.click('#action-btn');
  await page.click('#upload-apk');
  await page.getByText('Upload APK').click();

  expect(page.getByText('Drag & Drop File here')).toBeTruthy();

  const fileChooserPromise = page.waitForEvent('filechooser');
  await page.click('#add-file-btn');

  const fileChooser = await fileChooserPromise;
  await fileChooser.setFiles(path.resolve('test.apk'));

  await page.click('#confirm-file');

  await page.waitForResponse(
    async (resp) =>
      (resp.url().includes('https://firebasestorage.googleapis.com') &&
        (await resp.headerValue('x-goog-upload-status'))?.includes('final')) ??
      false,
    { timeout: 50000 }
  );
  await page.waitForLoadState('load');

  expect(page.getByText('No Upload in progress')).toHaveCount(1);

  await page.goto('http://localhost:3000/');
  await page.click('#action-btn');
  await page.click('#view');
  await expect(page).toHaveURL(/.*app-package/);

  await page.waitForLoadState('networkidle');

  await sleep(2000);

  const linkEl = page.locator('#download-apk');
  const href = await linkEl.getAttribute('href');
  expect(href).toContain('https://firebasestorage.googleapis.com');
});

test('Delete an app package', async ({ page }) => {
  await page.goto('http://localhost:3000/');
  expect(page.getByText('Test App')).toBeTruthy();

  await page.click('#action-btn');
  await page.click('#delete-app');
  await page.click('#dialog-confirm');

  expect(await page.getByTitle('Updated Test App').count()).toEqual(0);
});
