import { expect } from '@playwright/test';
import type { Page } from '@playwright/test';

export const loginUser = async (page: Page) => {
  await page.goto('localhost:3000/login');

  const nameInput = page.getByLabel('Email Address');
  const passwordInput = page.getByLabel('Password');

  await nameInput.fill('marcos.hardy@outlook.com');
  await passwordInput.fill('123456');

  await page.getByRole('button', { name: 'Sign In' }).click();

  // Expects the URL to contain intro.
  const successMsg = page.getByText('Successfully logged in');
  const dashboardHeader = page.getByText('App Packages');

  expect(successMsg).toBeTruthy();
  expect(dashboardHeader).toBeTruthy();

  await page.waitForURL('http://localhost:3000/');
  // Alternatively, you can wait until the page reaches a state where all cookies are set.
  await expect(page.getByText('Add Package')).toBeVisible();
};

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
