import { test, expect } from '@playwright/test';

test('invalid login credentials', async ({ page }) => {
  await page.goto('localhost:3000/login');

  const nameInput = page.getByLabel('Email Address');
  const passwordInput = page.getByLabel('Password');

  await nameInput.fill('invalid-email');
  await passwordInput.fill('incorrect-password');

  await page.getByRole('button', { name: 'Sign In' }).click();

  // Expects the URL to contain intro.
  const errorMsg = page.getByText(
    'Error signing in. Please check your email or password and try again.'
  );

  expect(errorMsg).toBeTruthy();
});

test('successfull login', async ({ page }) => {
  await page.goto('localhost:3000/login');

  const nameInput = page.getByLabel('Email Address');
  const passwordInput = page.getByLabel('Password');

  await nameInput.fill('marcos.hardy@outlook.com');
  await passwordInput.fill('123456');

  await page.getByRole('button', { name: 'Sign In' }).click();

  // Expects the URL to contain intro.
  const successMsg = page.getByText('Successfully logged in');
  const dashboardHeader = page.getByText('App Packages');

  expect(successMsg).toBeTruthy();
  expect(dashboardHeader).toBeTruthy();
});
