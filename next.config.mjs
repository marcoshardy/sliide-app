/** @type {import('next').NextConfig} */
const nextConfig = {
  pageExtensions: ["page.tsx", "api.ts"],
  images: {
    unoptimized: true,

    // to be removed once using proper images
    domains: ["*"],
  },
  reactStrictMode: true,
};

export default nextConfig;
